import numpy as np
from keras.models import Sequential
from keras.layers import Dense

from sklearn.linear_model import LogisticRegression
logisticRegr = LogisticRegression()




def generate_data(n):
	
	#prva klasa
	n1 = int(n/2)
	x1_1 = np.random.normal(0.0, 2, (n1,1));
	
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
	
	#druga klasa
	n2 = int(n - n/2)
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)
	
	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]
	
	return data

data_traning = generate_data(500)
data_testing = generate_data(100)

x_data_traning = data_traning[:,0:2]
y_data_traning = data_traning[:,2].reshape(-1,1)

x_data_testing = data_testing[:,0:2]
y_data_testing = data_testing[:,2].reshape(-1,1)

model = Sequential()
model.add(Dense(4, input_dim=2, activation='sigmoid'))
model.add(Dense(4, activation='sigmoid'))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_data_traning, y_data_traning, epochs=150, batch_size=10)

_, accuracy = model.evaluate(x_data_traning, y_data_traning)
print('Accuracy on traning data: %.2f' % (accuracy*100))

_, accuracy = model.evaluate(x_data_testing, y_data_testing)
print('Accuracy on testing data: %.2f' % (accuracy*100))

logisticRegr = LogisticRegression()
logisticRegr.fit(x_data_traning, y_data_traning)
score = logisticRegr.score(x_data_testing, y_data_testing)
print('Accuracy of Logistic Regression on testing data: %.2f' % (score*100))