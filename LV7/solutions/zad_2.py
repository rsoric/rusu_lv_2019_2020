import numpy as np

from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold

import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T

def baseline_model():
	model = Sequential()
	model.add(Dense(13, input_dim=1, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, kernel_initializer='normal'))
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model

data_traning = non_func(100)
X = data_traning[:,0].reshape(-1,1)
Y = data_traning[:,1].reshape(-1,1)

data_testing = non_func(30)
X_test = data_testing[:,0].reshape(-1,1)
Y_test = data_testing[:,1].reshape(-1,1)

estimator = KerasRegressor(build_fn=baseline_model, epochs=100, batch_size=5, verbose=0)
kfold = KFold(n_splits=10)
results = cross_val_score(estimator, X, Y, cv=kfold)
print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))

regr = linear_model.LinearRegression()
regr.fit(X, Y)
regr_y_pred = regr.predict(X_test)
print('Linear regression model coefficients: \n', regr.coef_)
print('MSE: %.2f'% mean_squared_error(Y_test, regr_y_pred))
print('Coefficient of determination: %.2f'% r2_score(Y_test, regr_y_pred))


