from sklearn import datasets
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state,centers=4)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data1 = generate_data(500,1)
data2 = generate_data(500,2)
data3 = generate_data(500,3)
data4 = generate_data(500,4)
data5 = generate_data(500,5)

fig, axs = plt.subplots(3, 2)

kmeans = KMeans(n_clusters=4)
kmeans.fit(data1)
y_kmeans = kmeans.predict(data1)
axs[0, 0].scatter(data1[:, 0], data1[:, 1], c=y_kmeans, s=50, cmap='viridis')
centers = kmeans.cluster_centers_
axs[0, 0].scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)

kmeans = KMeans(n_clusters=4)
kmeans.fit(data2)
y_kmeans = kmeans.predict(data2)
axs[1, 0].scatter(data2[:, 0], data2[:, 1], c=y_kmeans, s=50, cmap='viridis')
centers = kmeans.cluster_centers_
axs[1, 0].scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)

kmeans = KMeans(n_clusters=4)
kmeans.fit(data3)
y_kmeans = kmeans.predict(data3)
axs[0, 1].scatter(data3[:, 0], data3[:, 1], c=y_kmeans, s=50, cmap='viridis')
centers = kmeans.cluster_centers_
axs[0, 1].scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)

kmeans = KMeans(n_clusters=4)
kmeans.fit(data4)
y_kmeans = kmeans.predict(data4)
axs[1, 1].scatter(data4[:, 0], data4[:, 1], c=y_kmeans, s=50, cmap='viridis')
centers = kmeans.cluster_centers_
axs[1, 1].scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)

kmeans = KMeans(n_clusters=4)
kmeans.fit(data5)
y_kmeans = kmeans.predict(data5)
axs[2, 1].scatter(data5[:, 0], data5[:, 1], c=y_kmeans, s=50, cmap='viridis')
centers = kmeans.cluster_centers_
axs[2, 1].scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)