from sklearn import datasets
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans


def generate_data(n_samples, flagc):
    if flagc == 1:
        random_state = 365
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)

    elif flagc == 2:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state, centers=4)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)

    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)

    else:
        X = []

    return X



data3 = generate_data(500, 3)

criterionValues = []
kValues = []

for k in range(1,20):
    kmeans = KMeans(n_clusters=k)
    kmeans.fit(data3)
    y_kmeans = kmeans.predict(data3)
    criterionValues.append(kmeans.inertia_)
    kValues.append(k)

plt.plot(kValues,criterionValues)
plt.show()

#optimalni broj K je 'elbow' ili lakat na grafu, što možemo očitati da je 4
#to je očekivano zbog toga što smo broj klastera postavili na 4