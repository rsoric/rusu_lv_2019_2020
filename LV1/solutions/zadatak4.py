import math

inputCounter = 0
inputMin = 0
inputMax = 0
inputSum = 0

while(1):

    user_input=input("Unesite broj: ")

    if(user_input == "Done"):
        break

    try:
        inputNumber = float(user_input)
        inputCounter = inputCounter + 1
        inputSum = inputSum + inputNumber

        if(inputCounter == 1):
            inputMax = inputNumber
            inputMin = inputNumber
        
        if(inputNumber>inputMax):
            inputMax = inputNumber
        
        if(inputNumber<inputMin):
            inputMin = inputNumber        

    except ValueError:
        print("Unos nije broj.")

inputAverage = inputSum / inputCounter

print("Broj unosa: ",inputCounter)
print("Minimalna vrijednost: ",inputMin)
print("Maksimalna vrijednost: ",inputMax)
print("Prosječna vrijednost unosa: ",inputAverage)
