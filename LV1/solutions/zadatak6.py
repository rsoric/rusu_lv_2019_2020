fhand = open("/home/student/Documents/rusu labosi/rusu_lv_2019_2020/LV1/solutions/mbox-short.txt")

emails = []

for line in fhand:
    line = line.rstrip()
    if line.startswith('From:'):
        email = line.split(':')[1].strip()
        emails.append(email)

print(emails[0])
print(emails[1])
print(emails[2])
print(emails[3])

domains = []

for email in emails:
    domain = email.split('@')[1]
    if domain in domains:
        continue
    else:
        domains.append(domain)

emailhosts = {}

for domain in domains:
    domaincounter = 0
    for email in emails:
        if email.split('@')[1] == domain:
            domaincounter = domaincounter+1
    emailhosts[domain] = domaincounter

print (emailhosts)