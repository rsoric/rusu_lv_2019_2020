import math

user_input = input("Unesite ocjenu između 0.0 i 1.0: ")

try:
    grade = float(user_input)

except ValueError:
    print("Unos nije broj.")

if(grade>=0.9 and grade <=1):
    print("Ocjena je u kategoriji A")
elif (grade>=0.8 and grade <0.9):
    print("Ocjena je u kategoriji B")
elif (grade>=0.7 and grade <0.8):
    print("Ocjena je u kategoriji C")
elif (grade>=0.6 and grade <0.7):
    print("Ocjena je u kategoriji D")
elif (grade>=0.0 and grade <0.6):
    print("Ocjena je u kategoriji F")
else:
    print("Broj nije rasponu 0.0 do 1.0")