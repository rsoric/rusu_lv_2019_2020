from sklearn.linear_model import LogisticRegression

import numpy as np
import matplotlib.pyplot as plt

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

np.random.seed(242)
data_learn = generate_data(200)

np.random.seed(12)
data_test = generate_data(100)



logisticRegression = LogisticRegression()

logisticRegression.fit(data_learn[:,:2],data_learn[:,2])

predictedValues= logisticRegression.predict(data_test[:,:2])

theta0 = logisticRegression.intercept_
theta1 = logisticRegression.coef_

x1 = np.arange(-8,8,0.5)
x2 = []

for i in range(len(x1)):
    x2value = -theta0-theta1*x1[i]/theta1
    x2.append(x2value[:,0])

x1 = np.asarray(x1)
x2 = np.asarray(x2)

x1 = x1.reshape(-1,1)
x2 = x2.reshape(-1,1)

print(predictedValues)
actualValues = data_test[:,2]

correctFlag = []
for i in range(len(predictedValues)):
    if(predictedValues[i]==actualValues[i]):
        correctFlag.append(1)
    else:
        correctFlag.append(0)

print(correctFlag)
plt.scatter(data_test[:,0],data_test[:,1], c=["black","green"],cmap=correctFlag)
plt.show()