from sklearn import datasets
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import mean_squared_error

import numpy as np
import pandas as pd

#Visualization Libraries
import seaborn as sns
import matplotlib.pyplot as plt

boston = datasets.load_boston()
bostondf = pd.DataFrame(boston.data)
bostondf.columns = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT']
bostondf['MEDV'] = boston.target

#prikaz distribucije vrijednosti cijena
sns.distplot(bostondf['MEDV'])
plt.show()

#prikaz korelacija između veličina
correlation_matrix = bostondf.corr().round(2)
sns.heatmap(data=correlation_matrix, annot=True)
plt.show()

#primjećujemo korelaciju između broja soba i cijene
#i negativnu korelaciju između postotak nižeg statusa populacije i cijene

#slijedi prikaz ove dvije korelacije

plt.figure(figsize=(20, 5))
plt.subplot(1, 2, 1)
x = bostondf['RM']
y = bostondf['MEDV']
plt.scatter(x, y, marker='o')
plt.title("Variation in House prices")
plt.xlabel('RM')
plt.ylabel('"Cijene kuća (po $1000)"')

plt.subplot(1, 2, 2)
x = bostondf['LSTAT']
y = bostondf['MEDV']
plt.scatter(x, y, marker='o')
plt.title("Variation in House prices")
plt.xlabel('LSTAT')
plt.ylabel('"Cijene kuća (po $1000)"')

plt.show()


#razdvajanje podataka 70-30 za učenje i test
X_rooms = bostondf.RM
y_price = bostondf.MEDV

X_rooms = np.array(X_rooms).reshape(-1,1)
y_price = np.array(y_price).reshape(-1,1)

X_train_1, X_test_1, Y_train_1, Y_test_1 = train_test_split(X_rooms, y_price, test_size = 0.3, random_state=5)

reg_1 = LinearRegression()
reg_1.fit(X_train_1, Y_train_1)

y_train_predict_1 = reg_1.predict(X_train_1)
rmse = (np.sqrt(mean_squared_error(Y_train_1, y_train_predict_1)))

print('Srednja kvadratna pogreška modela je:{}'.format(rmse))
print("\n")

#plottanje pravca dobivenog linearnog modela
prediction_space = np.linspace(min(X_rooms), max(X_rooms)).reshape(-1,1) 
plt.scatter(X_rooms,y_price)
plt.plot(prediction_space, reg_1.predict(prediction_space), color = 'red', linewidth = 2)
plt.xlabel('RM')
plt.ylabel('"Cijene kuća (po $1000)"')

plt.show()