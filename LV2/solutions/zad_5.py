import csv
import pprint
import numpy as np
import matplotlib.pyplot as plt


csvFile = open("/home/student/Documents/rusu labosi/rusu_lv_2019_2020/LV2/resources/mtcars.csv",'r')
dict_reader = csv.DictReader(csvFile)
cars = []
for line in dict_reader:
    cars.append(line)

pprint.pprint(cars)

mpgValues = []

for car in cars:
    horsepower = float(car['hp'])
    milesPerGallon = float(car['mpg'])
    label = str(car['wt'])
    plt.plot(horsepower,milesPerGallon,'bo')
    plt.text(horsepower,milesPerGallon,label)
    mpgValues.append(milesPerGallon)

mpgMin = str(min(mpgValues))
mpgMax = str(max(mpgValues))
mpgAverage = str(sum(mpgValues)/len(mpgValues))
plt.xlabel('Min mpg: '+mpgMin+' Max mpg: '+mpgMax+' Avg mpg: '+mpgAverage)
plt.show()

