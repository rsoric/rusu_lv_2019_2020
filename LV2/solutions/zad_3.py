import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
np.random.seed(5003)

genders = np.random.randint(0,2,100)
heights = []

sumOfMaleHeights = 0
numOfMales = 0
sumOfFemaleHeights = 0
numOfFemales = 0

plt.hlines(1,1,200) 
plt.xlim(150,200)
plt.ylim(0.5,1.5)

for gender in genders:
    if(gender==0):
        height = np.random.normal(180,7)
        heights.append(height)
        plt.plot(height,1,'|',ms='60',color='blue')
        sumOfMaleHeights = sumOfMaleHeights + height
        numOfMales = numOfMales+1
    else:
        height = np.random.normal(167,7)
        heights.append(height)
        plt.plot(height,1,'|',ms='60',color='red')
        sumOfFemaleHeights = sumOfFemaleHeights + height
        numOfFemales = numOfFemales + 1

averageMaleHeight = sumOfMaleHeights / numOfMales
averageFemaleHeight = sumOfFemaleHeights / numOfFemales

plt.xlabel('Avg. Male: '+str(averageMaleHeight)+', Avg. Female: '+str(averageFemaleHeight))

plt.show()

#Ne razumijem dio zadatka gdje traži da koristimo mp.dot kako bi izračunali srednje vrijednosti
#Izračunao sam ih na način na koji znam


