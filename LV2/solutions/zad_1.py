# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 08:16:40 2020

@author: student
"""
import re

fhand = open("D:/Student/rsoric/rusu lab/rusu_lv_2019_2020/LV2/resources/mbox-short.txt")

emails = []

for line in fhand:
    line = line.rstrip()
    if line.startswith('From:'):
        email = line.split(':')[1].strip()
        emails.append(email)

usernames = []

for email in emails:
    username = re.search('[^@]+',email)
    usernames.append(username.group())

print (usernames)