# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 08:16:40 2020

@author: student
"""
import re

fhand = open("D:/Student/rsoric/rusu lab/rusu_lv_2019_2020/LV2/resources/mbox-short.txt")

emails = []

for line in fhand:
    line = line.rstrip()
    if line.startswith('From:'):
        email = line.split(':')[1].strip()
        emails.append(email)

usernames = []

print ("Sadrži barem jedno slovo a:")

for email in emails:
    username = re.search('[^@]+',email)
    username = username.group()
    if re.search('a',username):
        usernames.append(username)

print (usernames)
           
##

usernames = []

print ("Sadrži točno jedno slovo a:")

for email in emails:
    username = re.search('[^@]+',email)
    username = username.group()
    if len(re.findall('a',username))==1:
        usernames.append(username)

print (usernames)

##

usernames = []

print ("Ne sadrži slovo a:")

for email in emails:
    username = re.search('[^@]+',email)
    username = username.group()
    if len(re.findall('a',username))==0:
        usernames.append(username)

print (usernames)

##

usernames = []

print ("Sadrži jedan ili više numeričkih znakova:")

for email in emails:
    username = re.search('[^@]+',email)
    username = username.group()
    if re.search('[0-9]+',username):
        usernames.append(username)

print (usernames)

##

usernames = []

print ("Sadrži samo mala slova:")

for email in emails:
    username = re.search('[^@]+',email)
    username = username.group()
    if not(re.search('[A-Z]+',username)):
        usernames.append(username)

print (usernames)

