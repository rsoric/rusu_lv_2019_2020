import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 

mtcars = pd.read_csv('mtcars.csv')

mpgSortedMtcars = mtcars.sort_values(by='mpg', ascending=False)
print("Auti s najvecom potrosnjom: ")
print(mpgSortedMtcars[['car','mpg']].head(5))

mpg8CilMtcars = mtcars.loc[mtcars['cyl']==8]
print("Auti s najmanjom potrosnjom s 8 cilindara: ")
print(mpg8CilMtcars[['car','mpg','cyl']].tail(5))

mpg6CilMtcars = mtcars.loc[mtcars['cyl']==6]
print("Srednja potrosnja auti sa 6 cilindara: ")
print(mpg6CilMtcars['mpg'].mean())

mpg4CilMtcarsWeightClass = mtcars.loc[(mtcars['cyl'] == 4) & (mtcars['wt'] < 2200) & (mtcars['wt'] > 2200)]
print("Srednja potrosnja auti s 4 cilindra od 2000 i 2200 lbs: ")
print(mpg4CilMtcarsWeightClass['mpg'].mean())

onlyAutomaticMtcars =  mtcars.loc[(mtcars['am'] == 1)]
print("Broj auta s automatskim mjenjacem: ")
print(len(onlyAutomaticMtcars))

onlyStickShiftMtcars =  mtcars.loc[(mtcars['am'] == 0)]
print("Broj auta s rucnim mjenjacem: ")
print(len(onlyStickShiftMtcars))

onlyAutomatic100hpMtcars = onlyAutomaticMtcars.loc[(mtcars['hp'] > 100)]
print("Broj auta s automatskim mjenjacem preko 100hp: ")
print(len(onlyAutomatic100hpMtcars))

print("Masa svakog automobila u kg: ")
print(mtcars[['wt']]*453.59237)