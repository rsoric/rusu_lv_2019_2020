import urllib.request
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import pprint


# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=1.1.2018'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek
df['date'] = df['vrijeme'].dt.date

#mjerenje to float

# dohvaćanje mjerenja
print(df)

# 3 dana s najvecom koncentracijom
print(df.sort_values(['mjerenje'], ascending=False).head(3))

# mjeseci u kojima fali najviše dana
missingDays = pd.date_range(start = '2017-01-01', end = '2018-01-01' ).difference(df['date'])

missingDaysByMonth = [0,0,0,0,0,0,0,0,0,0,0,0]

for missingDay in missingDays:
    monthOfMissingDay = missingDay.month
    missingDaysByMonth[monthOfMissingDay-1]+=1

# prikaz mjeseca kojima fali najviše dana
fig = plt.figure()
ax1 = fig.add_axes([0,0,1,1])
ax1.set_title('Mjeseci kojima fali najvise dana')
months = ['sječanj', 'veljača', 'ožujak', 'travanj', 'svibanj','lipanj','srpanj','kolovoz','rujan','listopad','studeni','prosinac']
ax1.bar(months,missingDaysByMonth)


#boxplot 1. i 7. mjeseca

januaryValues = df.loc[df['month']==1]
januaryValues = januaryValues['mjerenje'].values

julyValues = df.loc[df['month']==7]
julyValues = julyValues['mjerenje'].values

januaryAndJulyValues = [januaryValues,julyValues]
fig = plt.figure(figsize =(10, 7))  
plt.boxplot(januaryAndJulyValues) 

# usporedba distribucije radnih dana i vikenda
workingDayValues = df.loc[(df['dayOfweek']==0) | (df['dayOfweek']==1) | (df['dayOfweek']==2) | (df['dayOfweek']==3) | (df['dayOfweek']==4)]
workingDayValues = workingDayValues['mjerenje'].values

weekendValues = df.loc[(df['dayOfweek']==5) | (df['dayOfweek']==6)]
weekendValues = weekendValues['mjerenje'].values

workingDayAndWeekendValues = [workingDayValues, weekendValues]
fig = plt.figure(figsize =(10, 7))  
plt.boxplot(workingDayAndWeekendValues) 

plt.show() 

