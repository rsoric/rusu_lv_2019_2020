import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 

mtcars = pd.read_csv('mtcars.csv')

mpg8CilMtcars = mtcars.loc[mtcars['cyl']==8]
mpg6CilMtcars = mtcars.loc[mtcars['cyl']==6]
mpg4CilMtcars = mtcars.loc[mtcars['cyl']==4]

meanMpg = [
    mpg4CilMtcars['mpg'].mean(),
    mpg6CilMtcars['mpg'].mean(),
    mpg8CilMtcars['mpg'].mean()]

plt.bar([4,6,8],meanMpg)
plt.show()

weights = [mpg4CilMtcars['wt'],mpg6CilMtcars['wt'],mpg8CilMtcars['wt']]
fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
bp = ax.boxplot(weights)
plt.show()

onlyAutomaticMtcars =  mtcars.loc[(mtcars['am'] == 1)]
onlyStickShiftMtcars =  mtcars.loc[(mtcars['am'] == 0)]

plt.bar(['Automatic', 'Stick'],[onlyAutomaticMtcars['mpg'].mean(),onlyStickShiftMtcars['mpg'].mean()])
plt.show()

for index, car in mtcars.iterrows():
    if car['am']==1:
        plt.plot(car['qsec'],car['hp'],'bo')
    else:
        plt.plot(car['qsec'],car['hp'],'go')
plt.show()